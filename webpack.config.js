/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path');
const { CheckerPlugin } = require('awesome-typescript-loader');
const TerserPlugin = require('terser-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
  mode: isProduction ? 'production' : 'development',
  entry: path.join(__dirname, './src/main.ts'),
  target: 'web',
  output: {
    filename: 'app.js',
    path: path.join(__dirname, './public')
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  },
  plugins: [
    new CheckerPlugin()
  ],
  optimization: {
    minimize: isProduction,
    minimizer: [
      new TerserPlugin({
        extractComments: false,
        cache: '.cache',
        terserOptions: {
          output: {
            comments: false
          }
        }
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.css|\.scss$/,
        use: [
          {
            loader: 'lit-scss-loader',
            options: {
              minify: true
            }
          },
          'extract-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.ts?$/,
        loader: 'awesome-typescript-loader',
        options: {
          useCache: true,
          cacheDirectory: '.cache',
          reportFiles: [
            'src/**/*.ts'
          ]
        }
      }
    ]
  }
};
