export const getCurrentlySelectedDate = (): Date => new Date(new URL(window.location.href).searchParams.get('date') ?? Date.now());
