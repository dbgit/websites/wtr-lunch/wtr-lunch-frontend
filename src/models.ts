export interface LocationResponse {
  name: string;
  date: string;
  menus: LocationMenu[];
}

export interface LocationMenu {
  name: string;
  price?: string;
  details: string[];
}

export enum WeekDay {
  Sunday,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday
}

export type MappedLocationObj = Record<string, MappedLocation>;

export interface OverviewResponse {
  locations: MappedLocationObj;
}

export interface MappedLocation {
  href: string;
  details: MappedLocationDetails;
}

export interface MappedLocationDetails {
  name: string;
  website: string;
  openOn: WeekDay[];
}
