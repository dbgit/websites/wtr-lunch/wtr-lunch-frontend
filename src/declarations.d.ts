/* eslint-disable import/no-default-export */
declare module '*.scss' {
  import { CSSResult } from 'lit-element';

  const content: CSSResult;
  export default content;
}
