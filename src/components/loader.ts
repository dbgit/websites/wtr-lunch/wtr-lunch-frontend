import { html, TemplateResult } from 'lit-element';

export const loader: TemplateResult = html`<div class="loader"></div>`;
