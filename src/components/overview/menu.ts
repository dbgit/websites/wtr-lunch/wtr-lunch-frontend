import { LitElement, html, property, customElement, TemplateResult, CSSResult } from 'lit-element';
import { LocationMenu } from '../../models';
import sharedStyles from './overview.scss';

@customElement('lunch-menu')
export class Menu extends LitElement {
  @property({ type: Object }) menuInfo!: LocationMenu;

  static get styles(): CSSResult[] {
    return [
      sharedStyles
    ];
  }

  render(): TemplateResult {
    return html`
      <div class="menu">
        <h2 class="menu-title">${this.menuInfo.name}</h2>
        <b class="menu-price">${this.menuInfo.price}</b>
        <ul class="menu-list">
          ${this.menuInfo.details.map(item => html`<li class="menu-item">${item}</li>`)}
        </ul>
      </div>
      `;
  }
}
