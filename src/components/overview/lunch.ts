import { LitElement, html, property, customElement, TemplateResult, CSSResult } from 'lit-element';
import { MappedLocationObj, OverviewResponse } from '../../models';
import sharedStyles from './overview.scss';
import { loader } from '../loader';
import { getCurrentlySelectedDate } from '../../utils';

@customElement('lunch-app')
export class LunchApp extends LitElement {
  @property({ type: Object }) locations!: MappedLocationObj;

  static get styles(): CSSResult[] {
    return [
      sharedStyles
    ];
  }

  async firstUpdated(): Promise<void> {
    const requestUrl = new URL('https://lunch.aws.boerlage.me/location');
    const requestDate = getCurrentlySelectedDate()
      .toISOString()
      .slice(0, 10);
    requestUrl.searchParams.set('date', requestDate);
    const res = await fetch(requestUrl.toString());
    const { locations } = await res.json() as OverviewResponse;
    this.locations = locations;
  }

  render(): TemplateResult {
    if (!this.locations) {
      return loader;
    }

    return html`<div>${Object.values(this.locations).map(location => html`<lunch-location .location=${location}>`)}</div>`;
  }
}
