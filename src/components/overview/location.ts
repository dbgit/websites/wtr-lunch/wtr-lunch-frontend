import { LitElement, html, property, customElement, TemplateResult, CSSResult } from 'lit-element';
import { LocationResponse, MappedLocation } from '../../models';
import sharedStyles from './overview.scss';
import { loader } from '../loader';

@customElement('lunch-location')
export class Location extends LitElement {
  @property({ type: Object }) location!: MappedLocation;
  @property({ type: Object }) info!: LocationResponse;

  static get styles(): CSSResult[] {
    return [
      sharedStyles
    ];
  }

  async firstUpdated(): Promise<void> {
    const res = await fetch(this.location.href);
    this.info = await res.json() as LocationResponse;
  }

  render(): TemplateResult {
    const menus = html`<div class="menu-container">${this.info?.menus?.map(menu => html`<lunch-menu .menuInfo=${menu}>`)}</div>`;

    return html`
      <div class="location">
        <h1 class="location-name">${this.location.details.name}</h1>
        ${!this.info ? loader : menus}
      </div>
      `;
  }
}
