import { LitElement, html, customElement, TemplateResult, CSSResult } from 'lit-element';
import { getCurrentlySelectedDate } from '../../utils';
import styles from './toolbar.scss';

@customElement('lunch-toolbar')
export class Toolbar extends LitElement {
  static get styles(): CSSResult[] {
    return [
      styles
    ];
  }

  getDates(): Date[] {
    const week: Date[] = [];
    const today = new Date();
    today.setDate((today.getDate() - today.getDay() + 1));

    [ ...Array(7).keys() ].forEach(() => {
      week.push(new Date(today));
      today.setDate(today.getDate() + 1);
    });

    return week;
  }

  dateToListItem(date: Date, currentlySelectedDate: Date): TemplateResult {
    const item = date.toLocaleDateString('de-CH', { weekday: 'long' });
    const formatted = date.toISOString().slice(0, 10);

    const url = new URL(window.location.href);
    url.searchParams.set('date', formatted);

    if (date.getDay() === currentlySelectedDate.getDay()) {
      return html`<li><a class="active" href="${url.toString()}">${item}</a></li>`;
    }

    return html`<li><a href="${url.toString()}">${item}</a></li>`;
  }

  render(): TemplateResult {
    const currentlySelectedDate = getCurrentlySelectedDate();

    return html`
      <ul class="dates-row">
        ${this.getDates().map(date => this.dateToListItem(date, currentlySelectedDate))}
      </ul>
      `;
  }
}
